﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m3_2
{
    class Program
    {
        //Реализовать метод "пузырьковой" сортировки для целочисленного 
        //    массива(не использовать методы класса System.Array) таким образом, чтобы была возможность упорядочить строки матрицы
        static void Main(string[] args)
        {
            int n, buf;
            n = 5;
            string str;
            int[,] a = new int[n,n];
            Random rand = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {                    
                    a[i,j] = rand.Next(0, 10);
                    Console.Write(a[i, j] + " ");
                }
                Console.WriteLine();
            }
           str= summass(a,n);
            show(a, n, str);
            str = maxmass(a, n);
            show(a, n, str);
            str = minmass(a, n);
            show(a, n, str);

            Console.ReadKey();
        }
        static void show(int[,] a, int n,string str)
        {
            string[] s = str.Split(' ');
            Console.WriteLine("Ascending: ");
            for (int j = 0; j < s.Length; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    Console.Write(" " + a[Convert.ToInt32(s[j]), i]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Descending: ");
            for (int j = s.Length-1; j >=0; j--)
            {
                for (int i = 0; i < n; i++)
                {
                    Console.Write(" " + a[Convert.ToInt32(s[j]), i]);
                }
                Console.WriteLine();
            }
        }
            static string sort(int[,] a, int n)
        {
            int buf;
            int num;
            string str="";
            for (int k = n - 1; k > 0; k--)
                for (int i = 0; i < k; i++)
                    if (a[0,i] > a[0,i + 1])
                    {
                        buf = a[0,i];
                        num = a[1, i];
                        a[0,i] = a[0,i + 1];
                        a[1, i] = a[1, i + 1];
                        a[0,i + 1] = buf;
                        a[1, i + 1] = num;
                    }
            for (int i = 0; i < n; i++)
            {
                str += a[1, i]+" ";
            }
            str = str.Remove(str.Length - 1);
                return str;
        }
        //в порядке возрастания(убывания) сумм элементов строк матрицы;
        static string summass(int[,] mass, int n)
        {
            Console.WriteLine("in ascending(descending) order of sums of matrix row elements");
            int[,] otv = new int[2,n];
            for (int i = 0; i < n; i++)
            {
                otv[0,i] = 0;
                otv[1, i] = i;
                for (int j = 0; j < n; j++)
                {
                    otv[0,i] += mass[i, j];
            
                }
                //Console.WriteLine(otv[0,i]);
            }
        
            return sort(otv, n);
        }
        //в порядке возрастания(убывания) максимальных элементов строк матрицы;
        static string maxmass(int[,] mass, int n)
        {
            Console.WriteLine("in ascending(descending) order of maximum elements of matrix rows; ");
            int[,] otv = new int[2, n];
            for (int i = 0; i < n; i++)
            {
                otv[0, i] = mass[0, 0];
                otv[1, i] = i;

                for (int j = 0; j < n; j++)
                {
                    if(otv[0, i] < mass[i,j])
                        otv[0, i] = mass[i, j];

                }
               
               // Console.WriteLine(otv[0,i]);
            }

            return sort(otv, n);
        }
       // в порядке возрастания(убывания) минимальных элементов строк матрицы.
        static string minmass(int[,] mass, int n)
        {
            Console.WriteLine("in ascending(descending) order of the minimum elements of the rows of the matrix.");
            int[,] otv = new int[2, n];
            for (int i = 0; i < n; i++)
            {
                otv[0, i] = mass[0, 0];
                otv[1, i] = i;

                for (int j = 0; j < n; j++)
                {
                    if (otv[0, i] > mass[i, j])
                        otv[0, i] = mass[i, j];

                }

               // Console.WriteLine(otv[0, i]);
            }

            return sort(otv, n);
        }

    }
}
