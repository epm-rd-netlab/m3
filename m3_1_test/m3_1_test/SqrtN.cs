﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m3_1_test
{
    public class SqrtN
    {
        static double Pow(double a, int pow)
        {
            double result = 1;
            for (int i = 0; i < pow; i++) result *= a;
            return result;
        }

        public double metodSqrtN(double A, double power, double eps)
        {
            if (power < 0 || eps < 0)
                return -1;
            var x0 = A / power;
            var x1 = (1 / power) * ((power - 1) * x0 + A / Pow(x0, (int)power - 1));

            while (Math.Abs(x1 - x0) > eps)
            {
                x0 = x1;
                x1 = (1 / power) * ((power - 1) * x0 + A / Pow(x0, (int)power - 1));
            }

            return x1;
        }
    }
}
