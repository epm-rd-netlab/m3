﻿using System;
using m3_1_test;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMetho1()
        {
            //int[] number = new int[] { 2, 0, 1, 7 };
            SqrtN test = new SqrtN();
            double actual = test.metodSqrtN(1, 5, 0.0001);
            Assert.AreEqual(1, Math.Round(actual, 3));

        }
        [TestMethod]
        public void TestMetho2()
        {
            //int[] number = new int[] { 2, 0, 1, 7 };
            SqrtN test = new SqrtN();
            double actual = test.metodSqrtN(8, 3, 0.0001);
            Assert.AreEqual(2, Math.Round(actual, 3));

        }
        [TestMethod]
        public void TestMetho3()
        {
            //int[] number = new int[] { 2, 0, 1, 7 };
            SqrtN test = new SqrtN();
            double actual = test.metodSqrtN(0.001, 3, 0.0001);
            Assert.AreEqual(0.1, Math.Round(actual, 3));

        }
        [TestMethod]
        public void TestMetho4()
        {
            //int[] number = new int[] { 2, 0, 1, 7 };
            SqrtN test = new SqrtN();
            double actual = test.metodSqrtN(0.04100625, 4, 0.0001);
            Assert.AreEqual(0.45, Math.Round(actual, 3));

        }
        [TestMethod]
        public void TestMetho5()
        {
            //int[] number = new int[] { 2, 0, 1, 7 };
            SqrtN test = new SqrtN();
            double actual = test.metodSqrtN(8, 3, 0.0001);
            Assert.AreEqual(2, Math.Round(actual, 3));

        }
        [TestMethod]
        public void TestMetho6()
        {
            //int[] number = new int[] { 2, 0, 1, 7 };
            SqrtN test = new SqrtN();
            double actual = test.metodSqrtN(0.0279936, 7, 0.0001);
            Assert.AreEqual(0.6, Math.Round(actual, 3));

        }
        [TestMethod]
        public void TestMetho7()
        {
            //int[] number = new int[] { 2, 0, 1, 7 };
            SqrtN test = new SqrtN();
            double actual = test.metodSqrtN(0.0081, 4, 0.1);
            Assert.AreEqual(0.36, Math.Round(actual, 3));

        }
        [TestMethod]
        public void TestMetho8()
        {
            //int[] number = new int[] { 2, 0, 1, 7 };
            SqrtN test = new SqrtN();
            double actual = test.metodSqrtN(-0.008, 3, 0.1);
            Assert.AreEqual(-0.22, Math.Round(actual, 3));

        }
        [TestMethod]
        public void TestMetho9()
        {
            //int[] number = new int[] { 2, 0, 1, 7 };
            SqrtN test = new SqrtN();
            double actual = test.metodSqrtN(0.001, -2, 0.0001);
            Assert.AreEqual(-1, Math.Round(actual, 3));

        }
        [TestMethod]
        public void TestMetho10()
        {
            //int[] number = new int[] { 2, 0, 1, 7 };
            SqrtN test = new SqrtN();
            double actual = test.metodSqrtN(0.01, 2, -1);
            Assert.AreEqual(-1, Math.Round(actual, 3));

        }


    }
}
